install.packages("DBI")
install.packages("odbc")
library("rstudioapi")

library(odbc)
odbcListDrivers()


con <- DBI::dbConnect(odbc::odbc(),
                      Driver   = "/opt/microsoft/msodbcsql17/lib64/libmsodbcsql-17.4.so.1.1",
                      Server   = "initiativetracker-p-db.its.utas.edu.au",
                      Database = "Initiative_Tracker",
                      UID      = rstudioapi::askForPassword("Database user"),
                      PWD      = rstudioapi::askForPassword("Database password"),
                      Port     = 1433)